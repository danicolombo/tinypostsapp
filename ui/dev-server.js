const {createServer} = require('http')
const httpProxy = require('http-proxy')
const next = require('next')
const {parse} = require('url')

const {
    NODE_ENV,
    WEB_SERVICE_HOST = 'localhost',
    WEB_SERVICE_PORT = '8000',
} = process.env

const dev = NODE_ENV !== 'production'
const app = next({dev})
const handle = app.getRequestHandler()

const backendPaths = ['/api', '/admin', '/uploads']
const shouldProxy = pathname => backendPaths.some(path => pathname.startsWith(path))

app.prepare().then(() => {
    const proxy = httpProxy.createProxyServer({
        target: `http://${WEB_SERVICE_HOST}:${WEB_SERVICE_PORT}`,
    })

    proxy.on('error', (error, req, res) => {
        res.end(error.toString())
    })

    const server = createServer((req, res) => {
        const parsedUrl = parse(req.url, true)
        const {pathname} = parsedUrl

        if (shouldProxy(pathname)) proxy.web(req, res)
        else handle(req, res, parsedUrl)
    })

    server.listen(3000, error => {
        if (error) throw error
        console.info('> Ready on http://localhost:3000')
    })
})