export const SITE_NAME = 'BlogApp'
export const NAME_AND_DOMAIN = 'BlogApp'
export const WEB_SERVICE = 'http://localhost:8000'
export const LOGIN_URL = '/users/login'
export const LOGOUT_URL = '/users/logout'