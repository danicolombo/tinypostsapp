export type User = {
    id: string
    username: string
    email: string
}

export type crsfResponse = {
    csrf: string
}

export type UserResponse = User

export type UsersResponse = User[]

export type LoginRequest = {
    email: string
    password: string
}

export type LoginResponse = {
    id: string
    username: string
    email: string
    token: string
}
