import React, {FC} from 'react'
import {LoginRequest} from '../types/users'
import {Button, makeStyles, Grid} from '@material-ui/core'
import {Formik, Form as FormikForm, FormikErrors} from 'formik'
import {TextInput} from './input'
import {FormErrors} from './form-errors'


const useStyles = makeStyles((theme) => ({
    margins: {
        margin: '5px 0px',
        fontFamily: 'Roboto Slab', fontSize: '20px', fontWeight: 700, lineHeight: '26px', letterSpacing: '0.25px' 
    },
}))

type LoginFormProps = {
    onSubmit(values: LoginRequest): void
}

export const LoginForm: FC<LoginFormProps> = ({ onSubmit }) => {
    const classes = useStyles()

    const initialValues = {
        email: "",
        password: ""
    }

    const validate = (values) => {
        let errors: FormikErrors<LoginRequest> = {};
        if (values.email === '') {
            errors.email = 'Este campo es requerido'
        }
        if (values.password === '') {
            errors.email = 'Este campo es requerido'
        }
        return errors
    };

    return (
        <Formik 
            initialValues={initialValues}
            validateOnChange={false}
            validate={validate}
            onSubmit={onSubmit}>
            <FormikForm >
                <Grid container justify='center' direction='column'>
                    <TextInput
                        name='email'
                        label='Email'
                        type='email'
                        gridProps={{ xs: 12 }}
                        className={classes.margins}
                    />
                    <TextInput
                        name='password'
                        label='Password'
                        type='password'
                        gridProps={{ xs: 12 }}
                        className={classes.margins}
                    />
                    <FormErrors />
                    <Button
                        type='submit'
                        color='secondary'
                        variant='contained'>
                        Ingresar
                    </Button>
                </Grid>
            </FormikForm>
        </Formik>
    )
}
