import React, {FC, useState} from 'react'
import {PostCreateRequest, PostRequest, Topic} from '../types/posts'
import {Button, Grid, makeStyles, TextField} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete/Autocomplete'
import {Formik, Form as FormikForm, Field, FormikErrors} from 'formik'
import {TextInput} from './input'
import {FormErrors} from './form-errors'


const useStyles = makeStyles((theme) => ({
    margins: {
        margin: '10px 0px',
    },
    photoButton: {
        marginRight: '10px',
    },
    button: {
        margin: '10px 0px',
    }
}))

interface PostFormProps {
    initialValues?: Partial<PostRequest>
    onSubmit(values: PostCreateRequest): void
    onCancel?(): void
    topics?: Topic[]
}

export const PostForm: FC<PostFormProps> = ({ onSubmit, onCancel, topics }) => {
    const classes = useStyles()
    const [postimage, setPostImage] = useState<any>(null)
    const [value, setValue] = useState<any>(topics[0])
    const [inputValue, setInputValue] = useState('')

    const initialValues: PostCreateRequest = {
        title: '',
        content: '',
        description: '',
        image: undefined,
        topic: undefined,
    }

    const handleChange = (e) => {
        if (e.target.name === "image") {
            setPostImage(e.target.files[0])
        }
    }

    const validate = (values) => {
        let errors: FormikErrors<PostCreateRequest> = {};
        if (values.title === '') {
            errors.title = 'Este campo es requerido'
        }
        if (values.description === '') {
            errors.description = 'Este campo es requerido'
        }
        if (values.content === '') {
            errors.content = 'Este campo es requerido'
        }
        return errors
    };

    return (
        <Formik
            onSubmit={ values => {
                values.image = postimage
                values.topic = value.id
                onSubmit(values)
            }}
            validateOnChange={false}
            validate={validate}
            initialValues={initialValues}>
                <FormikForm>
                    <Grid container justify='center' direction='column'>
                    <TextInput
                        name='title'
                        label='T&iacute;tulo'
                        inputMode='text'
                        gridProps={{ xs: 12 }}
                        className={classes.margins}
                    />
                    <TextInput
                        name='description'
                        label='Descripci&oacute;n'
                        inputMode='text'
                        multiline={true}
                        rows={4}
                        gridProps={{ xs: 12 }}
                        className={classes.margins}
                    />
                    <TextInput
                        name='content'
                        label='Contenido'
                        inputMode='text'
                        multiline={true}
                        rows={4}
                        gridProps={{ xs: 12 }}
                        className={classes.margins}
                    />
                    <Field name="topic">
                        {({ field, meta }) => {
                            return (
                                <Autocomplete
                                    {...field}
                                    id="topic"
                                    options={topics}
                                    value={value}
                                    onChange={(event, newValue: string | null) => {
                                        setValue(newValue)
                                    }}
                                    inputValue={inputValue}
                                    onInputChange={(event, newInputValue) => {
                                        setInputValue(newInputValue)
                                    }}                                    
                                    className={classes.margins}
                                    getOptionLabel={(option: Topic) => option.name}
                                    renderInput={(params) => <TextField {...params} label="Topic" variant="outlined" />}
                                />
                            )
                        }}
                    </Field>
                    <Field name="image">
                        {({ field }) => {
                            return (
                                <label htmlFor="image">
                                    <input {...field}
                                        style={{ display: 'none' }}
                                        id="image"
                                        accept="image/*"
                                        onChange={handleChange}
                                        name="image"
                                        type="file"
                                    />
                                    <Button color="secondary" variant="contained" component="span" className={classes.photoButton}>
                                        Subir imagen
                                    </Button>
                                    {postimage !== null ? postimage.name : ''}
                                </label>
                            )
                        }}
                    </Field>
                    <FormErrors />
                    <Button
                        className={classes.margins}
                        type='submit'
                        color='secondary'
                        variant='contained'>
                        Guardar
                    </Button>
                    {onCancel && <Button onClick={onCancel}>Cancelar</Button>}
                    </Grid>
                </FormikForm>
        </Formik>
    )
}
