import React from 'react'
import {makeStyles, Card, CardActions, CardContent, Button, Typography} from '@material-ui/core'
import {useRouter} from 'next/router'


const useStyles = makeStyles({
    root: {
        minWidth: 275,
        borderBottom: '1px solid grey',
        boxShadow: 'unset !important',
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 18,
        color: '#00bff3',
    },
    button: {
        fontSize: 18,
        textTransform: 'uppercase',
        fontFamily: 'Elderkin',
    },
    pos: {
        marginBottom: 12,
    },
    noMargins: {
        padding: '0px !important',
    },
});

const OutlinedCard: React.FC<{
    card: any;
}> = ({ card }) => {
    const classes = useStyles();
    const router = useRouter()

    return (
        <Card className={classes.root} >
            <CardContent>
                <CardActions className={classes.noMargins}>
                    <Typography className={classes.title} color="textSecondary" variant='subtitle2'>
                    {card.title} 
                </Typography>
                    <Button size="small" className={classes.button} onClick={() => router.push(`${card.link}`)}>Learn More</Button>
                </CardActions>
                <Typography variant="h4" component="h2">
                    {card.description}
                </Typography>
                <Typography variant='body2' component="p">
                    {card.content}
                </Typography>
            </CardContent>
        </Card>
    );
}

export default OutlinedCard;