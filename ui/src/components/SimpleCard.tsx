import React from 'react';
import {makeStyles, Card, CardContent, Typography} from '@material-ui/core';
import styles from '../../styles/Shared.module.css'


const useStyles = makeStyles({
    root: {
        minWidth: 275,
        boxShadow: 'unset !important',
        marginTop: '50px',
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

const SimpleCard: React.FC<{
    card: any;
}> = ({ card }) => {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;

    return (
        <Card className={classes.root} >
            <CardContent className={styles.image}>
                <Typography variant="subtitle1" component="h2" >
                    {bull}{card.title}{bull}
                </Typography>
                <Typography variant="body2" component="p">
                    {card.description}
                </Typography>
            </CardContent>
        </Card>
    );
}

export default SimpleCard;