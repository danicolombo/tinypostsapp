import React, {FC, ReactElement} from 'react'
import {Grid} from '@material-ui/core'
import {Post} from '../types/posts'
import HomePostCard from './HomePostCard'


type PreviewProps = {
    posts: Post[]
}

export const HomePreview: FC<PreviewProps> = ({ posts }): ReactElement => {
    return (
        <Grid container spacing={3} justify="flex-start">
            {posts?.map((post, index) => (
            <Grid key={index} item xs={12}>
                    <HomePostCard post={post} />
            </Grid>
            ))}
        </Grid>
    )
}
