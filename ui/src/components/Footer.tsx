import React, {FC, ReactElement} from 'react'
import {Typography} from '@material-ui/core'
import style from '../../styles/Footer.module.css'


export const Footer: FC = ({}): ReactElement => {
    return (
        <div className={style.footer}>
            <Typography variant="body1" display="inline" className={style['footer-content']}>
                Made with ♥ by {' '}
                <a
                    href="https://procnedc.github.io/resume/"
                    target="_blank"
                    rel="Daniela Colombo"
                >
                    DC
                </a>
            </Typography>
        </div>
    )
}
