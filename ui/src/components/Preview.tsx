import React, {FC, ReactElement} from 'react'
import {Grid} from '@material-ui/core'
import {Post} from '../types/posts'
import PreviewCard from './PreviewCard'


type PreviewProps = {
    posts: Post[]
}

export const Preview: FC<PreviewProps> = ({ posts }): ReactElement => {
    return (
            <Grid container spacing={3} justify="flex-start">
                {posts?.map((post, index) => (
                    <Grid key={index} item xs={12} md={6}>
                        <PreviewCard post={post} />
                    </Grid>
                ))}
            </Grid>
    )
}
