import React, {useEffect, useState} from 'react'
import {useAuth} from '../src/context/auth'
import {SITE_NAME} from '../src/types/constants'
import {api} from '../src/api'
import {PostsResponse, Topic} from '../src/types/posts'
import {NextPage} from 'next'
import {useRouter} from 'next/router'
import Head from 'next/head'
import {Grid, Typography, useMediaQuery, Link} from '@material-ui/core'
import {HomePreview} from '../src/components/HomePreview'
import {Feed} from '../src/components/Feed'
import OutlinedCard from '../src/components/OutlinedCard'
import SimpleCard from '../src/components/SimpleCard'
import TopBar from '../src/components/TopBar'
import data from '../src/assets/data.json';
import styles from '../styles/Shared.module.css'


const Home: NextPage = () => {
    const router = useRouter()
    const large = useMediaQuery('(min-width:700px)')
    const [loading, setLoading] = useState(true)
    const [posts, setPosts] = useState<PostsResponse>()
    const [topics, setTopics] = useState<Topic[]>([])
    const [tokenStorage, setToken] = useState('')
    const {authUser} = useAuth()

    useEffect(() => {
        const fetchPosts = async () => {
            const posts = await api.getPosts()
            setPosts(posts)
        }
        const fetchTopics = async () => {
            const topics = await api.getTopics()
            setTopics(topics)
        }
        fetchPosts()
        fetchTopics()

        const token = window.localStorage.getItem('token')
        setToken(token)
        setLoading(false)
    }, [])


    return (
        <>
            <Head>
                <title>{SITE_NAME}</title>
                <meta
                    name="description"
                    content={`${SITE_NAME} is an open source project developed with react, nextJS, typescript + Django Rest Framework`}
                />
                <meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no' />
            </Head>
            <TopBar />
            <Grid container>
                <nav className={styles.sectionlinks} data-editable="sectionLinks">
                    <a className={styles.sectionlink} onClick={() => router.push('/posts')}>Tiny posts</a>
                    <a href="https://kasfactory.net/" className={styles.sectionlink}>What is Kas Factory?</a>
                    <a href="https://procnedc.github.io/resume/" className={styles.sectionlinkportfolio}>What else?</a>
                    <a className={styles.sectionlinkblue} onClick={() => router.push('/posts/new')}>Just post</a>
                    {authUser?.token || tokenStorage !== '' ?
                        <Link href="/users/logout" className={styles.sectionlinklogout}>Logout</Link>
                        :
                        <Link href="/users/login" className={styles.sectionlinkblue}>Login</Link>
                    }
                </nav>
            </Grid>
            <Grid container>
                <Grid item md={12} lg={3} className={styles.arrowside}>
                    <Feed />
                    {data.slice(0,3).map((card: any) => (
                        <OutlinedCard key={card.title} card={card}/>
                    ))}
                </Grid>
                <Grid item md={12} lg={6} className={styles.headings}>
                <Typography className={styles.secondHeading} variant={large ? 'h3' : 'h6'}>A collaborative blog</Typography>
                    <Typography className={styles.secondHeading} variant='h5'>
                        I'm happy to see you here ʕ •ᴥ•ʔ
                    </Typography>
                    <Grid container>
                        <Grid item xs={12} className={styles.postContainer}>
                            <HomePreview posts={posts} />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={12} lg={3} className={styles.headings}>
                    {data.slice(3,).map((card: any) => (
                        <SimpleCard key={card.title} card={card} />
                    ))}
                </Grid>
            </Grid>
        </>
    )
}

export default Home
