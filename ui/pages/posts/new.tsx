import React, {useEffect, useState} from 'react'
import {Card, CardContent, CardHeader, makeStyles} from '@material-ui/core'
import {PostForm} from '../../src/forms/post-form'
import {NextPage} from 'next'
import {useRouter} from 'next/router'
import {useSnackbar} from 'notistack'
import {api} from '../../src/api'
import {PostCreateRequest, Topic} from '../../src/types/posts'
import {loginRequired} from '../../src/context/auth'
import TopBar from '../../src/components/TopBar'


const useStyles = makeStyles((theme) => ({
    card: {
        width: 800,
        [theme.breakpoints.down('md')]: {
            width: 800,
        },
        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
        margin: '10px auto',
        backgroundColor: theme.palette.primary.light,
        color: theme.palette.text.secondary,
        
    },
    titles: {
        fontFamily: 'Roboto Slab', fontSize: '20px', fontWeight: 700, lineHeight: '26px', letterSpacing: '0.25px' 
    }
}))

const CreatePost: NextPage = () => {
    const classes = useStyles()
    const [topics, setTopics] = useState<Topic[]>([])
    const router = useRouter()
    const {enqueueSnackbar} = useSnackbar()

    const handleSubmit = async (values: PostCreateRequest) => {
        const post = await api.createPost(values)
        enqueueSnackbar('Post creado!', { variant: 'success' })
        router.push(`/posts/${post.id}`)
    }

    useEffect(() => {
        const fetchTopics = async () => {
            const topics = await api.getTopics()
            setTopics(topics)
        }
        fetchTopics()
    }, [])

    const handleCancel = () => router.push('/')

    return (
        <>
        <TopBar />
            <Card variant='outlined' className={classes.card}>
                <CardHeader className={classes.titles}
                    title='Nuevo post' subtitle='Complet&aacute; el siguiente formulario para crear un nuevo post'
                />
                <CardContent >
                    <PostForm onSubmit={handleSubmit} onCancel={handleCancel} topics={topics}/>
                </CardContent>
            </Card>
        </>
    )
}

export default loginRequired(CreatePost)
