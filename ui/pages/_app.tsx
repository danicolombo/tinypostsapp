import React, {useEffect, useState} from 'react'
import App, {AppContext} from 'next/app'
import {AppProps} from 'next/app'
import {ThemeProvider} from '../src/theme'
import {AuthProvider} from '../src/context/auth'
import {Footer} from '../src/components/Footer'
import {SnackbarProvider} from 'notistack'
import styles from '../styles/App.module.css'


const MyApp = ({ Component, pageProps }: AppProps): React.ReactNode => {
  const [style, setStyle] = useState<React.CSSProperties>({ visibility: 'hidden' })
  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles)
    }
    setStyle({})
  }, [])

  return (
    <>
      <div className={styles['app-container']} style={style}>
        <ThemeProvider>
          <SnackbarProvider>
              <AuthProvider>
                <div className={styles['content-container']}>
                  <Component {...pageProps} />
                </div>
              <Footer />
            </AuthProvider>
          </SnackbarProvider>
        </ThemeProvider>
      </div>
    </>
  )
}

MyApp.getInitialProps = async (appContext: AppContext) => {
  const appProps = await App.getInitialProps(appContext)
  return { ...appProps }
}

export default MyApp
