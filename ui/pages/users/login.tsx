import React, {useState} from 'react'
import {useAuth} from '../../src/context/auth'
import {api} from '../../src/api'
import {NextPage} from 'next'
import {useRouter} from 'next/router'
import {LoginRequest} from '../../src/types/users'
import {CardContent, CardHeader, Card, makeStyles} from '@material-ui/core'
import {LoginForm} from '../../src/forms/login'
import {Alert} from '@material-ui/lab'
import TopBar from '../../src/components/TopBar'


const useStyles = makeStyles((theme) => ({
    card: {
        width: 400,
        [theme.breakpoints.down('md')]: {
            width: '100%',
        },
        margin: '10px auto',
        backgroundColor: theme.palette.primary.light,
        color: theme.palette.text.secondary,
    },
    titles: {
        fontFamily: 'Lato', fontSize: '40px', fontWeight: 700, lineHeight: '26px', letterSpacing: '0.25px'
    }
}))

const Login: NextPage = () => {
    const {setAuthUser} = useAuth()
    const [formErrors, setFormErrors] = useState('')
    const router = useRouter()
    const classes = useStyles()

    const handleSubmit = async (values: LoginRequest) => {
        const user = await api.login(values)

        if (user.token){
            window.localStorage.setItem('token', user.token)
            setAuthUser(user)
            setFormErrors('')
            const nextUrl = (router.query.next as string) || '/'
            router.push(nextUrl)
        } else {
            setFormErrors('Las credenciales son incorrectas')
        }
    }

    return (
        <>
            <TopBar />
            <Card variant='outlined' className={classes.card}>
                <CardHeader className={classes.titles}
                    title='Login'
                    subheader='Ingres&aacute; tu email y contrase&ntilde;a.'
                />
                {formErrors &&
                    <CardContent>
                        <Alert severity='error'>
                            {formErrors}
                        </Alert>
                    </CardContent>
                }
                <CardContent>
                    <LoginForm onSubmit={handleSubmit} />
                </CardContent>
            </Card>
        </>
    )
}

export default Login
