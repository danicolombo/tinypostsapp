import {useEffect} from 'react'
import {api} from '../../src/api'
import {useAuth} from '../../src/context/auth'
import {NextPage} from 'next'
import {useRouter} from 'next/router'

const Logout: NextPage = ({ }) => {
    const router = useRouter()
    const { setAuthUser } = useAuth()

    const logout = async () => {
        try {
            await api.logout()
        } catch {
            // noop
        }
        setAuthUser(undefined)
        window.localStorage.setItem('token', '')
        router.push('/')
    }

    useEffect(() => { logout() }, [])
    return null
}

export default Logout
