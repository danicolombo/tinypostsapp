from django.shortcuts import render

from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status

from .permissions import IsOwnerOrReadOnly
from .serializers import PostSerializer, TopicSerializer
from .models import Post, Topic


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    
    def get_queryset(self):
        topic = self.request.query_params.get('topic')
        if topic is not None:
            return self.queryset.filter(topic__id=topic)
        return Post.objects.all()

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class TopicViewSet(viewsets.ModelViewSet):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]



