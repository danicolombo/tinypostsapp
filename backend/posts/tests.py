from django.test import TestCase
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile

from users.models import User
from .models import Topic


class PostsTests(TestCase):
    def perform_create(self):
        Post = get_user_model()
        user = User.objects.create_user(email='normal@user.com', password='foo')
        topic = Topic.objects.create_topic(name='Tech')
        image_path = 'uploads/images/default.jpg'
        newPhoto = SimpleUploadedFile(name='default.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg')
        post = Post.objects.perform_create(title='Test', description='Test description', content='Test content', image=newPhoto, user=user, topic=topic)
        self.assertEqual(post.title, 'Test')
        self.assertEqual(post.description, 'Test description')
        self.assertEqual(post.content, 'Test content')

        with self.assertRaises(TypeError):
            Post.objects.perform_create()

        with self.assertRaises(TypeError):
            Post.objects.perform_create(title='')

        with self.assertRaises(ValueError):
            Post.objects.perform_create(image='')
