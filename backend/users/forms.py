from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import authenticate

from .models import User


class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = User
        fields = ('email',)


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('email',)


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    readonly_fields = ('id',)
    list_display = ('email', 'is_staff', 'is_active', 'id')
    list_filter = ('email', 'is_staff', 'is_active', 'id')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)


class AuthenticationForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()

    error_messages = {
        'inactive': 'This user is inactive.',
        'invalid_login': 'Please enter a correct email and password.',
    }

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        self.user = None
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')

        invalid_login = forms.ValidationError(self.error_messages['invalid_login'], code='invalid_login')

        if email and password:
            try:
                user = User.objects.get(email__iexact=email)
            except User.DoesNotExist:
                try:
                    user = User.objects.get(email__iexact=email)
                except User.DoesNotExist:
                    raise invalid_login

            if not user.is_active:
                raise forms.ValidationError(self.error_messages['inactive'], code='inactive')

            self.user = authenticate(self.request, username=user.email, password=password)
            if self.user is None:
                raise invalid_login

        return cleaned_data
