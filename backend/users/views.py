from django.contrib.auth import login, logout

from rest_framework import viewsets, serializers, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token

from .serializers import UserSerializer, LoginSerializer
from .models import User


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(detail=False, methods=['get'], permission_classes=[IsAuthenticated], url_path='active-user')
    def active_user(self, request):
        serializer = self.get_serializer(instance=request.user)
        return Response(serializer.data)

    @action(detail=False, methods=['post'], permission_classes=[], serializer_class=LoginSerializer)
    def login(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.get(email=request.data['email'])
        token, created = Token.objects.get_or_create(user=user)
        login(request, serializer.user)
        serializer = UserSerializer(instance=serializer.user)
        data = serializer.data
        data['token'] = token.key
        return Response(data)

    @action(detail=False, methods=['post'], permission_classes=[IsAuthenticated], serializer_class=serializers.Serializer)
    def logout(self, request):
        logout(request)
        return Response(status=status.HTTP_204_NO_CONTENT)
